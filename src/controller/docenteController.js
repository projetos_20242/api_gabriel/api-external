module.exports = class docenteController {
  //Listar Docentes
  static async infoDocente(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;

    console.log(nome);
    console.log(profissao);
    console.log(cursoMatriculado);
    console.log(idade);

    res.status(200).json({ message: "Cadastrado" });
  }

  static async editDocente(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    if (nome != "" && profissao != "" && cursoMatriculado != "" && idade != 0) {
      res.status(200).json({ message: "Editado com sucesso" });
    } 
    else {
      res.status(400).json({ message: "Dados em Branco" });
    }
  }

   //Deletar Aluno
   static async deletarAluno(req, res) {
    if (req.params.id != null){
        res.status(200).json({ message: "Aluno Excluido" });
    }
    else{
        res.status(400).json({ message: "Aluno não encontrado" });
    }
}
};
