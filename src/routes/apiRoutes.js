const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const docenteController = require('../controller/docenteController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')
//LEMBRE DO IMPORT

router.get('/ALGUMA-COISA/', teacherController.getAlgumaCoisa),
router.post('/infoDocente/', docenteController.infoDocente),
router.put('/editAluno/', docenteController.editDocente ),
router.delete('/deletAluno/:id', docenteController.deletarAluno);
router.get("/external/io", JSONPlaceholderController.getUsersWebsiteIO);

router.get("/external", JSONPlaceholderController.getUsers);
router.get("/getUsersWebsiteCOM", JSONPlaceholderController.getUsersWebsiteCOM);
router.get("/getUsersWebsiteNET", JSONPlaceholderController.getUsersWebsiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);

module.exports = router